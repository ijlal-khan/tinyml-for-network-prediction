import matplotlib.pyplot as plt
import logging
from data_handler import read_data
import joblib
from keras.models import load_model
from sklearn.metrics import mean_squared_error, mean_absolute_error
import numpy as np
import pandas as pd
import os
import tensorflow as tf
import sys
import contextlib
import io

# Suppress TensorFlow and Keras messages
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
tf.get_logger().setLevel('ERROR')

logging.basicConfig(level=logging.INFO, format='%(asctime)s %(message)s')

# Define constants
model_file = 'trained_model.h5'
scaler_file = 'scaler.gz'
sequence_length = 5
prediction_steps = 15  # 5 minutes (15 steps of 10 seconds each)

last_timestamp = None  # To keep track of the last timestamp
last_graph = None  # To store the last generated graph


@contextlib.contextmanager
def suppress_stdout():
    with open(os.devnull, 'w') as devnull:
        old_stdout = sys.stdout
        sys.stdout = devnull
        try:
            yield
        finally:
            sys.stdout = old_stdout


def create_sequences(data):
    sequences, targets = [], []
    for i in range(len(data) - sequence_length):
        sequence = data['Length'].iloc[i:i + sequence_length]
        target = data['Length'].iloc[i + sequence_length]
        sequences.append(sequence.astype(float))
        targets.append(target.astype(float))
    return np.array(sequences), np.array(targets)


def plot_predictions(df, true_values, predicted, extended_values=None, extended_timestamps=None):
    plt.figure(figsize=(10, 6))
    plt.plot(df['Timestamp'][:len(true_values)], true_values, label='Actual')
    plt.plot(df['Timestamp'][:len(predicted)], predicted, label='Predicted')
    if extended_values is not None:
        plt.plot(extended_timestamps, extended_values, label='Extended Predictions', linestyle='--')
    plt.xlabel('Time')
    plt.ylabel('Length')
    plt.title('Actual vs Predicted Values')
    plt.legend()
    # Save plot to a BytesIO object
    img = io.BytesIO()
    plt.savefig(img, format='png')
    img.seek(0)
    plt.close()
    return img


def extend_predictions(model, last_sequence, steps, last_timestamp, interval):
    predictions, timestamps = [], []
    for _ in range(steps):
        with suppress_stdout():
            prediction = model.predict(last_sequence.reshape(1, sequence_length, 1))
        predictions.append(prediction[0, 0])
        last_sequence = np.append(last_sequence[1:], prediction)
        last_timestamp += pd.Timedelta(seconds=interval)
        timestamps.append(last_timestamp)
    return predictions, timestamps


def check_new_data(df):
    global last_timestamp
    if last_timestamp is None:
        last_timestamp = df['Timestamp'].max()
        return True
    else:
        new_timestamp = df['Timestamp'].max()
        if new_timestamp > last_timestamp:
            last_timestamp = new_timestamp
            return True
        return False


def build_graph():
    global last_graph
    logging.info("Testing model...")
    df = read_data()
    if df.empty:
        logging.error("No data available for testing")
        return last_graph, False

    if not check_new_data(df):
        logging.info("No new data available")
        return last_graph, False

    df_resampled = df.resample('10s', on='Timestamp').sum().fillna(0).reset_index()

    # Load scaler and transform data
    scaler = joblib.load(scaler_file)
    df_resampled['Length'] = scaler.transform(df_resampled[['Length']])
    sequences, targets = create_sequences(df_resampled)
    sequences = sequences.reshape((sequences.shape[0], sequences.shape[1], 1))

    # Load model
    try:
        model = load_model(model_file)
    except Exception as e:
        logging.error(f"Error loading model: {e}")
        return last_graph, False

    # Make predictions
    with suppress_stdout():
        predictions = model.predict(sequences)

    # Calculate MSE and MAE
    mse = mean_squared_error(targets, predictions)
    mae = mean_absolute_error(targets, predictions)

    logging.info(f"Model testing completed. MSE: {mse}, MAE: {mae}")
    print(f"MSE: {mse}")
    print(f"MAE: {mae}")

    # Extend predictions
    last_sequence = sequences[-1].reshape(sequence_length)
    last_timestamp = df_resampled['Timestamp'].iloc[-sequence_length]
    extended_predictions, extended_timestamps = extend_predictions(model, last_sequence, prediction_steps,
                                                                   last_timestamp, 10)

    # Plot predictions and store in last_graph
    last_graph = plot_predictions(df_resampled, targets, predictions, extended_predictions, extended_timestamps)
    return last_graph, True


if __name__ == '__main__':
    last_graph, success = build_graph()
    if success:
        with open("prediction_plot.png", "wb") as f:
            f.write(last_graph.getbuffer())
        print("Graph saved as prediction_plot.png")
