import pandas as pd
import logging

# Configure logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s %(levelname)s %(message)s')

data_file = 'packet_data.csv'


def read_data(file_name='packet_data.csv'):
    try:
        df = pd.read_csv(file_name, parse_dates=['Timestamp'])
        logging.info(f"Data read: {df.shape} rows")
        return df
    except Exception as e:
        logging.error(f"Error reading data: {e}")
        return pd.DataFrame()


if __name__ == '__main__':
    data = read_data()
    print(data.head())
