from flask import Flask, request
import csv

app = Flask(__name__)


@app.route('/send_data', methods=['POST'])
def receive_data():
    data = request.get_json()
    # Save data to a CSV file
    with open('network_data.csv', mode='a', newline='') as file:
        writer = csv.writer(file)
        writer.writerow([data['field1'], data['field2'], data['field3']])
    return "Data received", 200


if __name__ == '__main__':
    app.run(port=5000)
