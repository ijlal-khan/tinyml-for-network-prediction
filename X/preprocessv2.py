import pandas as pd
import numpy as np
from sklearn.preprocessing import MinMaxScaler
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error
import datetime
import matplotlib.pyplot as plt

# Sample data
data = {
    "Timestamp": ["2024-05-22 15:21:21.282858", "2024-05-22 15:21:29.259571", "2024-05-22 15:21:29.259674",
                  "2024-05-22 15:21:29.259731", "2024-05-22 15:21:29.285160", "2024-05-22 15:21:29.516519",
                  "2024-05-22 15:21:29.516563", "2024-05-22 15:21:29.548908", "2024-05-22 15:22:15.012886",
                  "2024-05-22 15:22:15.012989", "2024-05-22 15:22:15.013037", "2024-05-22 15:22:15.037984",
                  "2024-05-22 15:23:36.307930", "2024-05-22 15:23:36.308068", "2024-05-22 15:23:36.308131",
                  "2024-05-22 15:23:36.372329"],
    "Length": [55, 1292, 1292, 1292, 1292, 1292, 1292, 1292, 1292, 1292, 1292, 1292, 1292, 1292, 1292, 1292]
}

df = pd.DataFrame(data)
df['Timestamp'] = pd.to_datetime(df['Timestamp'])

# Resample by 2-second intervals and sum packet lengths
df.set_index('Timestamp', inplace=True)
df_resampled = df.resample('2S').sum().fillna(0).reset_index()

# Normalize the data
scaler = MinMaxScaler()
df_resampled['Length'] = scaler.fit_transform(df_resampled[['Length']])

# Initialize lists to store predictions and actual values
predictions = []
actual = []

# Initial train data
train_size = 10  # Number of initial points to train on

for i in range(train_size, len(df_resampled)):
    # Train data up to the current point
    df_train = df_resampled.iloc[:i]

    # Prepare the training data
    X_train = df_train['Timestamp'].apply(lambda x: x.toordinal()).values.reshape(-1, 1)
    y_train = df_train['Length'].values

    # Train the model
    model = LinearRegression()
    model.fit(X_train, y_train)

    # Predict the next 2-second interval
    next_timestamp = df_resampled['Timestamp'].iloc[i]
    next_timestamp_ordinal = next_timestamp.toordinal()
    next_packet_size = model.predict([[next_timestamp_ordinal]])
    predictions.append(next_packet_size[0])

    # Store the actual value
    actual.append(df_resampled['Length'].iloc[i])

# Convert predictions and actual values back to original scale
predictions = scaler.inverse_transform(np.array(predictions).reshape(-1, 1)).flatten()
actual = scaler.inverse_transform(np.array(actual).reshape(-1, 1)).flatten()

# Plotting the actual and predicted data
plt.figure(figsize=(12, 6))
plt.plot(df_resampled['Timestamp'][train_size:], actual, label='Actual Data', color='blue')
plt.plot(df_resampled['Timestamp'][train_size:], predictions, label='Predicted Data', color='red')
plt.xlabel('Timestamp')
plt.ylabel('Packet Length')
plt.title('Actual vs Predicted Network Packet Lengths (2-Second Intervals)')
plt.legend()
plt.grid(True)
plt.show()
