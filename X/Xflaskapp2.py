from flask import Flask, jsonify, render_template
import pandas as pd
import numpy as np
from sklearn.preprocessing import MinMaxScaler
import datetime
import matplotlib.pyplot as plt
import io
import base64
from keras.models import Sequential, load_model, save_model
from keras.layers import Conv1D, MaxPooling1D, Flatten, RepeatVector, LSTM, Dense
import os
import joblib
import logging

# Configure logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s %(levelname)s %(message)s')

app = Flask(__name__)
data_file = 'packet_data.csv'
model_file = 'trained_model.h5'
scaler_file = 'scaler.gz'
sequence_length = 5
lock = Lock()

def read_data():
    try:
        df = pd.read_csv(data_file, parse_dates=['Timestamp'])
        logging.info(f"Data read: {df.shape} rows")
        return df
    except Exception as e:
        logging.error(f"Error reading data: {e}")
        return pd.DataFrame()

def create_sequences(data, seq_length):
    xs, ys = [], []
    for i in range(len(data) - seq_length):
        x = data['Length'].iloc[i:i + seq_length].astype(float)
        y = data['Length'].iloc[i + seq_length].astype(float)
        xs.append(x)
        ys.append(y)
    return np.array(xs), np.array(ys)

def create_model():
    model = Sequential([
        Conv1D(filters=64, kernel_size=2, activation='relu', input_shape=(sequence_length, 1)),
        MaxPooling1D(pool_size=2),
        Flatten(),
        RepeatVector(1),
        LSTM(25, activation='relu', return_sequences=True),
        LSTM(25, activation='relu'),
        Dense(1)
    ])
    model.compile(optimizer='adam', loss='mse')
    return model

def train_and_save_model():
    logging.info("Training model...")
    df = read_data()
    if df.empty:
        logging.error("No data available for training")
        return

    df_resampled = df.resample('2s', on='Timestamp').sum().fillna(0).reset_index()

    scaler = MinMaxScaler()
    df_resampled['Length'] = scaler.fit_transform(df_resampled[['Length']])
    X, y = create_sequences(df_resampled, sequence_length)
    X = X.reshape((X.shape[0], X.shape[1], 1))

    model = create_model()
    model.fit(X, y, epochs=10, verbose=1, validation_split=0.2)

    with lock:
        save_model(model, model_file)
        joblib.dump(scaler, scaler_file)
    logging.info("Model trained and saved.")

def create_plot(predictions, actual, timestamps):
    plt.figure(figsize=(12, 6))
    plt.plot(timestamps, actual, label='Actual Data', color='blue')
    plt.plot(timestamps, predictions, label='Predicted Data', color='red')
    plt.xlabel('Timestamp')
    plt.ylabel('Packet Length')
    plt.title('Actual vs Predicted Network Packet Lengths (2-Second Intervals)')
    plt.legend()
    plt.grid(True)
    buf = io.BytesIO()
    plt.savefig(buf, format='png')
    buf.seek(0)
    return base64.b64encode(buf.getvalue()).decode('utf8')

def predict_and_update():
    logging.info("Predicting and updating graph...")
    predictions, actual, timestamps = [], [], []
    df = read_data()
    if df.empty:
        logging.error("No data available for prediction")
        return None

    df_resampled = df.resample('2s', on='Timestamp').sum().fillna(0).reset_index()

    with lock:
        try:
            model = load_model(model_file)
            scaler = joblib.load(scaler_file)
        except Exception as e:
            logging.error(f"Error loading model or scaler: {e}")
            return None

    df_resampled['Length'] = scaler.transform(df_resampled[['Length']])

    for i in range(len(df_resampled) - sequence_length):
        start_idx = i
        end_idx = i + sequence_length
        current_sequence = df_resampled['Length'].iloc[start_idx:end_idx].values.reshape(
            (1, sequence_length, 1)).astype(float)
        next_packet_size = model.predict(current_sequence)
        predictions.append(next_packet_size[0])
        actual.append(df_resampled['Length'].iloc[end_idx])
        timestamps.append(df_resampled['Timestamp'].iloc[end_idx])
    graph_url = create_plot(predictions, actual, timestamps)
    logging.info("Graph updated successfully.")
    return graph_url

@app.route('/')
def index():
    logging.info("Serving index page")
    return render_template('index.html')

@app.route('/update_graph')
def update_graph():
    logging.info("update_graph called")
    graph_url = predict_and_update()
    if graph_url is None:
        logging.error("Failed to load model or scaler")
        return jsonify({'error': 'Failed to load model or scaler'}), 500
    logging.info("Graph URL generated successfully")
    return jsonify({'graph': graph_url})

if __name__ == '__main__':
    logging.info("Starting Flask app")
    # Start the initial training and save the model
    if not os.path.exists(model_file) or not os.path.exists(scaler_file):
        train_and_save_model()
    app.run(debug=True)
