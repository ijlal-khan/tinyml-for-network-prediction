import matplotlib.pyplot as plt
import logging

from keras import Sequential
from keras.layers import LSTM, Dense, Dropout
from sklearn.preprocessing import MinMaxScaler

from data_handler import read_data
import joblib
from keras.models import load_model, save_model
from sklearn.metrics import mean_squared_error, mean_absolute_error
import numpy as np
import pandas as pd
import os
import tensorflow as tf
import time
import sys
import contextlib

# Suppress TensorFlow and Keras messages
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
tf.get_logger().setLevel('ERROR')

logging.basicConfig(level=logging.INFO, format='%(asctime)s %(message)s')

# Define constants
model_file = 'trained_lstm_model.h5'
scaler_file = 'scaler.gz'
sequence_length = 5
prediction_steps = 15  # 5 minutes (15 steps of 10 seconds each)

last_timestamp = None  # To keep track of the last timestamp


@contextlib.contextmanager
def suppress_stdout():
    with open(os.devnull, 'w') as devnull:
        old_stdout = sys.stdout
        sys.stdout = devnull
        try:
            yield
        finally:
            sys.stdout = old_stdout


def create_sequences(data):
    sequences, targets = [], []
    for i in range(len(data) - sequence_length):
        sequence = data['Length'].iloc[i:i + sequence_length]
        target = data['Length'].iloc[i + sequence_length]
        sequences.append(sequence.astype(float))
        targets.append(target.astype(float))
    return np.array(sequences), np.array(targets)


def plot_predictions(df, true_values, predicted, extended_values=None, extended_timestamps=None):
    plt.figure(figsize=(10, 6))
    plt.plot(df['Timestamp'][:len(true_values)], true_values, label='Actual')
    plt.plot(df['Timestamp'][:len(predicted)], predicted, label='Predicted')
    if extended_values is not None:
        plt.plot(extended_timestamps, extended_values, label='Extended Predictions', linestyle='--')
    plt.xlabel('Time')
    plt.ylabel('Length')
    plt.title('Actual vs Predicted Values')
    plt.legend()
    plt.show()


def extend_predictions(model, last_sequence, steps, last_timestamp, interval):
    predictions, timestamps = [], []
    for _ in range(steps):
        with suppress_stdout():
            prediction = model.predict(last_sequence.reshape(1, sequence_length, 1))
        predictions.append(prediction[0, 0])
        last_sequence = np.append(last_sequence[1:], prediction)
        last_timestamp += pd.Timedelta(seconds=interval)
        timestamps.append(last_timestamp)
    return predictions, timestamps


def check_new_data(df):
    global last_timestamp
    if last_timestamp is None:
        last_timestamp = df['Timestamp'].max()
        return True
    else:
        new_timestamp = df['Timestamp'].max()
        if new_timestamp > last_timestamp:
            last_timestamp = new_timestamp
            return True
        return False


def build_graph():
    logging.info("Testing model...")
    df = read_data()
    if df.empty:
        logging.error("No data available for testing")
        return

    if not check_new_data(df):
        logging.info("No new data available")
        return

    df_resampled = df.resample('10s', on='Timestamp').sum().fillna(0).reset_index()

    # Load scaler and transform data
    scaler = joblib.load(scaler_file)
    df_resampled['Length'] = scaler.transform(df_resampled[['Length']])

    # Debug: Print scaled data
    logging.debug(f"Scaled data sample: {df_resampled[['Length']].head()}")

    sequences, targets = create_sequences(df_resampled)
    sequences = sequences.reshape((sequences.shape[0], sequences.shape[1], 1))

    # Load model
    model = load_model(model_file)

    # Make predictions
    with suppress_stdout():
        predictions = model.predict(sequences)

    # Inverse transform the predictions to get actual values
    predictions = scaler.inverse_transform(predictions)
    targets = scaler.inverse_transform(targets.reshape(-1, 1))

    # Calculate MSE and MAE
    mse = mean_squared_error(targets, predictions)
    mae = mean_absolute_error(targets, predictions)

    logging.info(f"Model testing completed. MSE: {mse}, MAE: {mae}")
    print(f"MSE: {mse}")
    print(f"MAE: {mae}")

    # Print actual and predicted values for debugging
    for i in range(10):  # Print first 10 values as a sample
        print(f"Actual: {targets[i][0]}, Predicted: {predictions[i][0]}")

    # Extend predictions
    last_sequence = sequences[-1].reshape(sequence_length)
    last_timestamp = df_resampled['Timestamp'].iloc[-sequence_length]
    extended_predictions, extended_timestamps = extend_predictions(model, last_sequence, prediction_steps,
                                                                   last_timestamp, 10)

    # Inverse transform the extended predictions to get actual values
    extended_predictions = scaler.inverse_transform(np.array(extended_predictions).reshape(-1, 1))

    # Debug: Print extended predictions
    for i in range(10):  # Print first 10 extended predictions
        print(f"Extended Prediction {i}: {extended_predictions[i][0]}")

    # Plot predictions
    plot_predictions(df_resampled, targets, predictions, extended_predictions, extended_timestamps)


def train_and_save_model(file='TrainData.csv', epochs=100, validation_split=0.2):
    """Trains and saves the model with hyperparameter options.

    Args:
        epochs (int, optional): Number of epochs to train. Defaults to 10.
        validation_split (float, optional): Fraction of data for validation. Defaults to 0.2.
        :param file:
    """

    logging.info("Training model...")
    df = read_data(file)
    if df.empty:
        logging.error("No data available for training")
        return

    df_resampled = df.resample('10s', on='Timestamp').sum().fillna(0).reset_index()

    scaler = MinMaxScaler()
    df_resampled['Length'] = scaler.fit_transform(df_resampled[['Length']])
    X, y = create_sequences(df_resampled)
    print(X.shape)
    X = X.reshape((X.shape[0], X.shape[1], 1))

    model = create_model(input_shape=(sequence_length, 1))

    # Adding EarlyStopping callback
    early_stopping = tf.keras.callbacks.EarlyStopping(monitor='val_loss', patience=10, restore_best_weights=True)

    history = model.fit(X, y, epochs=epochs, verbose=1, validation_split=validation_split, callbacks=[early_stopping])

    save_model(model, model_file)
    joblib.dump(scaler, scaler_file)
    logging.info("Model trained and saved.")

    # Plot training and validation loss curves
    plt.figure(figsize=(10, 6))
    plt.plot(history.history['loss'], label='Training Loss')
    plt.plot(history.history['val_loss'], label='Validation Loss')
    plt.xlabel('Epochs')
    plt.ylabel('Loss')
    plt.title('Training and Validation Loss')
    plt.legend()
    plt.show()


def create_model(input_shape):
    model = Sequential()
    model.add(LSTM(128, activation='relu', input_shape=input_shape, return_sequences=True))
    model.add(Dropout(0.2))
    model.add(LSTM(64, activation='relu', return_sequences=False))
    model.add(Dropout(0.2))
    model.add(Dense(32, activation='relu'))
    model.add(Dense(1))

    model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=0.001), loss='mse')
    return model


if __name__ == '__main__':
    print("Training and testing model...")
    train_and_save_model()
    while True:
        build_graph()
        time.sleep(5)
