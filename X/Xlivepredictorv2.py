from keras.models import load_model
import pandas as pd
import numpy as np
from sklearn.preprocessing import MinMaxScaler
import joblib
import matplotlib.pyplot as plt

model_file = 'trained_model.h5'
scaler_file = 'scaler.gz'
num_predictions = 30  # 30 predictions for 5 minutes ahead at 10s intervals

# Load your trained model
model = load_model(model_file)
scaler = joblib.load(scaler_file)

# Define the sequence length
sequence_length = 5


def create_sequences(data, seq_length):
    X = []
    y = []
    for i in range(len(data) - seq_length):
        X.append(data.iloc[i:i + seq_length].tolist())
        y.append(data.iloc[i + seq_length])
    return np.array(X), np.array(y)


def predict(df_resample, X, num_predictions):
    predictions = []
    # Start with the initial sequences from the dataset
    initial_sequence = X[0]  # Start with the first sequence in the dataset

    # Generate predictions for the length of the dataset and additional 5 minutes
    for i in range(num_predictions):
        pred = model.predict(initial_sequence.reshape((1, sequence_length, 1)))[0, 0]
        predictions.append(pred)

        new_sequence = np.append(initial_sequence[1:], pred)  # Ensure pred is appended as a 1D array
        initial_sequence = new_sequence

    print("Predictions (Scaled):")
    print(predictions[:10])
    return scaler.inverse_transform(np.array(predictions).reshape(-1, 1))


def plot_actual(df_resample):
    # Step 4: Plot the resampled data
    plt.figure(figsize=(12, 6))
    plt.plot(df_resampled.index, df_resampled['Length'], marker='o', linestyle='-')
    plt.title('Packet Size over Time')
    plt.xlabel('Timestamp')
    plt.ylabel('Packet Size')
    plt.grid(True)
    plt.show()


def plot_prediction_only(df_resample, X, predictions):
    # Assuming df_resample and X are already defined and preprocessed
    num_predictions = 30  # Example number of predictions

    # Create a time index for the predicted values
    last_date = df_resample.index[-1]
    prediction_dates = pd.date_range(start=last_date, periods=num_predictions + 1, freq='5T')[
                       1:]  # Assuming 5-minute intervals

    # Plot the predictions
    plt.figure(figsize=(12, 6))
    plt.plot(prediction_dates, predictions, label='Predictions', linestyle='--')
    plt.xlabel('Time')
    plt.ylabel('Predicted Values')
    plt.title('Time Series Predictions')
    plt.legend()
    plt.show()


def plot_both(df_resample, predictions, num_predictions):
    # Create a timestamp range for the predictions
    last_timestamp = df_resample.index[-1]
    prediction_timestamps = pd.date_range(start=last_timestamp, periods=num_predictions + 1, freq='5T')[1:]

    # Create a DataFrame for the predictions
    df_predictions = pd.DataFrame(predictions, index=prediction_timestamps, columns=['Predicted Length'])

    # Concatenate the original and predicted data
    df_combined = pd.concat([df_resample, df_predictions])

    # Plot the data
    plt.figure(figsize=(12, 6))
    plt.plot(df_resample.index, df_resample['Length'], label='Original Data')
    plt.plot(df_predictions.index, df_predictions['Predicted Length'], label='Predictions', linestyle='--')
    plt.xlabel('Timestamp')
    plt.ylabel('Packet Length')
    plt.legend()
    plt.title('Original Data and Predictions')
    plt.show()


if __name__ == '__main__':
    df = pd.read_csv('packet_data.csv', parse_dates=['Timestamp'])
    df.set_index('Timestamp', inplace=True)
    # Step 3: Resample the data to 10-second intervals and sum the packet sizes
    df_resampled = df.resample('10s').sum().fillna(0)

    # Scale the data for prediction
    df_resampled_scaled = df_resampled.copy()
    df_resampled_scaled['Length'] = scaler.transform(df_resampled[['Length']])

    X, y = create_sequences(df_resampled_scaled['Length'], sequence_length)
    predictions = predict(df_resampled_scaled, X, num_predictions)
    print("Predictions (Unscaled):")
    print(predictions)
    plot_actual(df_resampled)
    plot_prediction_only(df_resampled, X, predictions)
    plot_both(df_resampled, predictions, num_predictions)
