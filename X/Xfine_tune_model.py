import numpy as np
from sklearn.preprocessing import MinMaxScaler
from keras.models import Sequential, load_model, save_model
from keras.layers import Conv1D, MaxPooling1D, Flatten, RepeatVector, LSTM, Dense
from sklearn.metrics import mean_squared_error
import joblib
import logging
from data_handler import read_data

# Configure logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s %(levelname)s %(message)s')

model_file = 'trained_model.h5'
scaler_file = 'scaler.gz'
sequence_length = 5
performance_threshold = 0.01


def create_sequences(data, seq_length):
    xs, ys = [], []
    if len(data) - seq_length < 1:
        logging.error("Data is too short for sequence length.")
        return np.array([]), np.array([])

    for i in range(len(data) - seq_length):
        x = data['Length'].iloc[i:i + seq_length].astype(float)
        y = data['Length'].iloc[i + seq_length].astype(float)
        xs.append(x)
        ys.append(y)
    return np.array(xs), np.array(ys)


def preprocess_data(df):
    scaler = MinMaxScaler()
    df['Length'] = scaler.fit_transform(df[['Length']])
    X, y = create_sequences(df, sequence_length)
    X = X.reshape((X.shape[0], X.shape[1], 1))
    return X, y, scaler


def evaluate_model(model, X, y):
    predictions = model.predict(X)
    mse = mean_squared_error(y, predictions)
    return mse


def fine_tune_model(new_data, epochs=5, validation_split=0.2):
    logging.info("Fine-tuning model...")

    if new_data.empty:
        logging.error("No data available for fine-tuning")
        return "No data available for fine-tuning"

    new_data_resampled = new_data.resample('10s', on='Timestamp').sum().fillna(0).reset_index()
    X_new, y_new, new_scaler = preprocess_data(new_data_resampled)

    if X_new.size == 0 or y_new.size == 0:
        logging.error("Insufficient data after preprocessing for fine-tuning.")
        return "Insufficient data for fine-tuning"

    try:
        # Load the existing model
        model = load_model(model_file)
        # Evaluate the current model performance on new data
        current_mse = evaluate_model(model, X_new, y_new)
        logging.info(f"Current model MSE on new data: {current_mse}")

        # Fine-tune the model with new data
        history = model.fit(X_new, y_new, epochs=epochs, verbose=0, validation_split=validation_split)

        # Get validation loss from the last epoch
        val_loss = history.history['val_loss'][-1]
        logging.info(f"Validation loss after fine-tuning: {val_loss}")

        # Check if the new model's performance is better than the threshold
        if val_loss < current_mse and val_loss < performance_threshold:
            logging.info("Fine-tuned model meets performance criteria. Saving the model.")
            save_model(model, model_file)
            joblib.dump(new_scaler, scaler_file)
            return "Model fine-tuned and saved"
        else:
            logging.info("Fine-tuned model did not meet performance criteria. Keeping the old model.")
            return "No fine-tuning needed; original model retained"
    except Exception as e:
        logging.error(f"Error during fine-tuning: {e}")
        return f"Error during fine-tuning: {e}"


if __name__ == '__main__':
    df_new = read_data()  # Read new data for fine-tuning
    result = fine_tune_model(df_new)
    print(result)
