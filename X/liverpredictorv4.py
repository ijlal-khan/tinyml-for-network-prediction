import pandas as pd
import numpy as np
from sklearn.preprocessing import MinMaxScaler
import datetime
import matplotlib.pyplot as plt
from keras.models import Sequential
from keras.layers import Conv1D, MaxPooling1D, Flatten, RepeatVector, LSTM, Dense

# Read data from CSV file
df = pd.read_csv('packet_data.csv')
df['Timestamp'] = pd.to_datetime(df['Timestamp'])

# Resample by 2-second intervals and sum packet lengths
df.set_index('Timestamp', inplace=True)
df_resampled = df.resample('2s').sum().fillna(0).reset_index()  # Use '2s' instead of '2S'

# Normalize the data
scaler = MinMaxScaler()
df_resampled['Length'] = scaler.fit_transform(df_resampled[['Length']])


# Create sequences
def create_sequences(data, seq_length):
    xs = []
    ys = []
    for i in range(len(data) - seq_length):
        x = data['Length'].iloc[i:i + seq_length].astype(float)  # Ensure float type for input data
        y = data['Length'].iloc[i + seq_length].astype(float)  # Ensure float type for labels
        xs.append(x)
        ys.append(y)
    return np.array(xs), np.array(ys)


sequence_length = 30
X, y = create_sequences(df_resampled, sequence_length)
X = X.reshape((X.shape[0], X.shape[1], 1))

# Split the data
split_index = int(len(X) * 0.8)
X_train, X_test = X[:split_index], X[split_index:]
y_train, y_test = y[:split_index], y[split_index:]


# Define the model
def create_model():
    model = Sequential()
    model.add(Conv1D(filters=64, kernel_size=2, activation='relu', input_shape=(sequence_length, 1)))
    model.add(MaxPooling1D(pool_size=2))
    model.add(Flatten())
    model.add(RepeatVector(1))
    model.add(LSTM(25, activation='relu', return_sequences=True))
    model.add(LSTM(25, activation='relu'))
    model.add(Dense(1))
    model.compile(optimizer='adam', loss='mse')
    return model


model = create_model()
model.summary()

# Train the model initially
model.fit(X_train, y_train, epochs=10, verbose=1, validation_split=0.2)


# Function to update sequences with new data
def update_sequences(df, scaler, sequence_length):
    df_resampled = df.resample('2s').sum().fillna(0).reset_index()  # Use '2s' instead of '2S'
    df_resampled['Length'] = scaler.transform(df_resampled[['Length']])
    X, y = create_sequences(df_resampled, sequence_length)
    X = X.reshape((X.shape[0], X.shape[1], 1))
    return X, y


# Continuous prediction and retraining loop
predictions = []
actual = []
retrain_interval = 30  # Retrain after every 30 predictions (i.e., 1 minute)
count = 0

for i in range(split_index, len(df_resampled) - sequence_length):
    # Prepare the current sequence
    current_sequence = df_resampled['Length'].iloc[i - sequence_length:i].values.reshape(
        (1, sequence_length, 1)).astype(float)

    # Predict the next value
    next_packet_size = model.predict(current_sequence)
    predictions.append(next_packet_size[0])
    actual.append(df_resampled['Length'].iloc[i])

    # Update counter and check if retraining is needed
    count += 1
    if count == retrain_interval:
        # Update sequences with new data
        X, y = update_sequences(df, scaler, sequence_length)

        # Retrain the model
        model.fit(X, y, epochs=10, verbose=1, validation_split=0.2)

        # Reset the counter
        count = 0

# Convert predictions and actual values back to original scale
predictions = scaler.inverse_transform(np.array(predictions).reshape(-1, 1)).flatten()
actual = scaler.inverse_transform(np.array(actual).reshape(-1, 1)).flatten()

# Plotting the actual and predicted data
plt.figure(figsize=(12, 6))
plt.plot(df_resampled['Timestamp'][split_index + sequence_length:], actual, label='Actual Data', color='blue')
plt.plot(df_resampled['Timestamp'][split_index + sequence_length:], predictions, label='Predicted Data', color='red')
plt.xlabel('Timestamp')
plt.ylabel('Packet Length')
plt.title('Actual vs Predicted Network Packet Lengths (2-Second Intervals)')
plt.legend()
plt.grid(True)
plt.show()
