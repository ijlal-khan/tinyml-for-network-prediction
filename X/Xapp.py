from flask import Flask, jsonify, render_template
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import io
import base64
from keras.models import load_model
import joblib
import logging
from model_trainer_CNN_LSTM import train_and_save_model
from data_handler import read_data

# Configure logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s %(levelname)s %(message)s')

app = Flask(__name__)
model_file = 'trained_model.h5'
scaler_file = 'scaler.gz'
sequence_length = 5

def create_plot(predictions, actual, timestamps):
    plt.figure(figsize=(12, 6))
    plt.plot(timestamps, actual, label='Actual Data', color='blue')
    plt.plot(timestamps, predictions, label='Predicted Data', color='red')
    plt.xlabel('Timestamp')
    plt.ylabel('Packet Length')
    plt.title('Actual vs Predicted Network Packet Lengths (2-Second Intervals)')
    plt.legend()
    plt.grid(True)
    buf = io.BytesIO()
    plt.savefig(buf, format='png')
    buf.seek(0)
    return base64.b64encode(buf.getvalue()).decode('utf8')

def predict_and_update():
    logging.info("Predicting and updating graph...")
    predictions, actual, timestamps = [], [], []
    df = read_data()
    if df.empty:
        logging.error("No data available for prediction")
        return None

    df_resampled = df.resample('2s', on='Timestamp').sum().fillna(0).reset_index()

    try:
        model = load_model(model_file)
        scaler = joblib.load(scaler_file)
    except Exception as e:
        logging.error(f"Error loading model or scaler: {e}")
        return None

    df_resampled['Length'] = scaler.transform(df_resampled[['Length']])

    for i in range(len(df_resampled) - sequence_length):
        start_idx = i
        end_idx = i + sequence_length
        current_sequence = df_resampled['Length'].iloc[start_idx:end_idx].values.reshape(
            (1, sequence_length, 1)).astype(float)
        next_packet_size = model.predict(current_sequence)
        predictions.append(next_packet_size[0])
        actual.append(df_resampled['Length'].iloc[end_idx])
        timestamps.append(df_resampled['Timestamp'].iloc[end_idx])
    graph_url = create_plot(predictions, actual, timestamps)
    logging.info("Graph updated successfully.")
    return graph_url

@app.route('/')
def index():
    logging.info("Serving index page")
    return render_template('index.html')

@app.route('/update_graph')
def update_graph():
    logging.info("update_graph called")
    graph_url = predict_and_update()
    if graph_url is None:
        logging.error("Failed to load model or scaler")
        return jsonify({'error': 'Failed to load model or scaler'}), 500
    logging.info("Graph URL generated successfully")
    return jsonify({'graph': graph_url})

if __name__ == '__main__':
    logging.info("Starting Flask app")
    # Start the initial training and save the model
    if not os.path.exists(model_file) or not os.path.exists(scaler_file):
        train_and_save_model()
    app.run(debug=True)
