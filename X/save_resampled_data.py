import pandas as pd
from data_handler import read_data

df = read_data()
df_resampled = df.resample('10s', on='Timestamp').sum().fillna(0).reset_index()
save_resampled_data = df_resampled.to_csv('resampled_data.csv', index=False)
