import pandas as pd
import numpy as np
from sklearn.preprocessing import MinMaxScaler
import datetime
import matplotlib.pyplot as plt
from keras.models import Sequential
from keras.layers import Conv1D, MaxPooling1D, Flatten, RepeatVector, LSTM, Dense

# Read data from CSV file
df = pd.read_csv('packet_data.csv')

df['Timestamp'] = pd.to_datetime(df['Timestamp'])

# Resample by 2-second intervals and sum packet lengths
df.set_index('Timestamp', inplace=True)
df_resampled = df.resample('2S').sum().fillna(0).reset_index()

# Normalize the data
scaler = MinMaxScaler()
df_resampled['Length'] = scaler.fit_transform(df_resampled[['Length']])

# Convert timestamps to ordinal and reshape data for Conv1D
def create_sequences(data, seq_length):
    xs = []
    ys = []
    for i in range(len(data) - seq_length):
        x = data.iloc[i:i+seq_length, 1]  # Get the length values for sequence
        y = data.iloc[i+seq_length, 1]    # Get the next value as the label
        xs.append(x)
        ys.append(y)
    return np.array(xs), np.array(ys)

sequence_length = 30
X, y = create_sequences(df_resampled, sequence_length)
X = X.reshape((X.shape[0], X.shape[1], 1))

# Split the data
split_index = int(len(X) * 0.8)
X_train, X_test = X[:split_index], X[split_index:]
y_train, y_test = y[:split_index], y[split_index:]

# Define the model
model = Sequential()
model.add(Conv1D(filters=64, kernel_size=2, activation='relu', input_shape=(sequence_length, 1)))
model.add(MaxPooling1D(pool_size=2))
model.add(Flatten())
model.add(RepeatVector(1))
model.add(LSTM(25, activation='relu', return_sequences=True))
model.add(LSTM(25, activation='relu'))
model.add(Dense(1))
model.compile(optimizer='adam', loss='mse')
model.summary()

# Train the model
model.fit(X_train, y_train, epochs=10, verbose=1, validation_split=0.2)

# Make predictions
y_pred = model.predict(X_test)
y_pred_rescaled = scaler.inverse_transform(y_pred)
y_test_rescaled = scaler.inverse_transform(y_test.reshape(-1, 1))

# Plotting the actual and predicted data
plt.figure(figsize=(12, 6))
plt.plot(df_resampled['Timestamp'][split_index + sequence_length:], y_test_rescaled, label='Actual Data', color='blue')
plt.plot(df_resampled['Timestamp'][split_index + sequence_length:], y_pred_rescaled, label='Predicted Data', color='red')
plt.xlabel('Timestamp')
plt.ylabel('Packet Length')
plt.title('Actual vs Predicted Network Packet Lengths (2-Second Intervals)')
plt.legend()
plt.grid(True)
plt.show()
