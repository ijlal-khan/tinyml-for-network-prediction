"""This script tests the trained model using the test data and calculates the Mean Squared Error (MSE) and Mean
Absolute Error (MAE) metrics."""

import numpy as np
import pandas as pd
from sklearn.preprocessing import MinMaxScaler
from keras.models import load_model
from sklearn.metrics import mean_squared_error, mean_absolute_error
import joblib
import logging
from data_handler import read_data

model_file = 'trained_model.h5'
scaler_file = 'scaler.gz'
sequence_length = 5


def create_sequences(data, seq_length):
    xs, ys = [], []
    for i in range(len(data) - seq_length):
        x = data['Length'].iloc[i:i + seq_length].astype(float)
        y = data['Length'].iloc[i + seq_length].astype(float)
        xs.append(x)
        ys.append(y)
    return np.array(xs), np.array(ys)


def test_model():
    """Tests the model using MSE and MAE metrics."""

    logging.info("Testing model...")
    df = read_data()
    if df.empty:
        logging.error("No data available for testing")
        return

    df_resampled = df.resample('10s', on='Timestamp').sum().fillna(0).reset_index()

    # Load scaler and transform data
    scaler = joblib.load(scaler_file)
    df_resampled['Length'] = scaler.transform(df_resampled[['Length']])
    X, y = create_sequences(df_resampled, sequence_length)
    X = X.reshape((X.shape[0], X.shape[1], 1))

    # Load model
    model = load_model(model_file)

    # Make predictions
    predictions = model.predict(X)

    # Calculate MSE and MAE
    mse = mean_squared_error(y, predictions)
    mae = mean_absolute_error(y, predictions)

    logging.info(f"Model testing completed. MSE: {mse}, MAE: {mae}")
    print(f"MSE: {mse}")
    print(f"MAE: {mae}")


if __name__ == '__main__':
    print("Testing model...")
    test_model()
