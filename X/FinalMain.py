import threading
import time
from model_trainer_CNN_LSTM import fine_tune_model
from live_monitoring import build_graph
from data_handler import read_data


def display_data_loop():
    while True:
        time.sleep(5)  # Monitor for 5 seconds
        try:
            build_graph()
        except Exception as e:
            print(f"Not Enough Data: {e}")


def fine_tune_model_loop():
    while True:
        print("Fine-tuning model...")
        time.sleep(60)  # Fine-tune model every 60 seconds
        try:
            fine_tune_model()
        except Exception as e:
            print(f"Error fine-tuning model: {e}")


if __name__ == '__main__':
    print("Starting monitoring...")
    # Create and start threads for both loops
    thread_display_data = threading.Thread(target=display_data_loop)
    thread_fine_tune = threading.Thread(target=fine_tune_model_loop)

    thread_display_data.start()
    thread_fine_tune.start()
