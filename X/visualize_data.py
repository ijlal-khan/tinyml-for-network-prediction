import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
import re

# Sample packet data
data = """
Packet: 130.231.168.124 -> 224.0.0.251, Protocol: MDNS, Length: 113
Packet: 130.231.162.57 -> 239.255.255.250, Protocol: SSDP, Length: 217
Packet: 130.231.168.124 -> 224.0.0.252, Protocol: LLMNR, Length: 69
Packet: 130.231.174.205 -> 224.0.0.252, Protocol: IGMP, Length: 46
Packet: 130.231.174.205 -> 224.0.0.252, Protocol: IGMP, Length: 46
Packet: 130.231.169.201 -> 239.255.255.250, Protocol: SSDP, Length: 218
Packet: 130.231.164.183 -> 224.0.0.251, Protocol: MDNS, Length: 978
Packet: 130.231.161.16 -> 224.0.0.251, Protocol: MDNS, Length: 1125
Packet: 130.231.169.234 -> 239.255.255.250, Protocol: SSDP, Length: 217
Packet: 130.231.172.25 -> 239.255.255.250, Protocol: SSDP, Length: 217
Packet: 130.231.162.57 -> 239.255.255.250, Protocol: SSDP, Length: 217
Packet: 130.231.175.187 -> 239.255.255.250, Protocol: SSDP, Length: 217
Packet: 130.231.169.229 -> 224.0.0.251, Protocol: MDNS, Length: 251
Packet: 130.231.172.13 -> 130.231.175.255, Protocol: DATA, Length: 82
Packet: 130.231.172.16 -> 239.255.255.250, Protocol: SSDP, Length: 218
Packet: 130.231.162.199 -> 224.0.0.251, Protocol: MDNS, Length: 201
"""

# Function to parse the packet data
def parse_packet_data(data):
    packet_list = []
    for line in data.strip().split("\n"):
        match = re.match(r'Packet: (\d+\.\d+\.\d+\.\d+) -> (\d+\.\d+\.\d+\.\d+), Protocol: (\w+), Length: (\d+)', line)
        if match:
            packet = {
                "Source": match.group(1),
                "Destination": match.group(2),
                "Protocol": match.group(3),
                "Length": int(match.group(4))
            }
            packet_list.append(packet)
    return packet_list

packet_list = parse_packet_data(data)
df = pd.DataFrame(packet_list)

# Add a timestamp column for plotting
df['Timestamp'] = pd.date_range(start='2024-01-01', periods=len(df), freq='S')

# Set up the plot
fig, ax = plt.subplots()
line, = ax.plot([], [], lw=2)
ax.set_xlim(df['Timestamp'].min(), df['Timestamp'].max())
ax.set_ylim(df['Length'].min() - 50, df['Length'].max() + 50)
ax.set_xlabel('Time')
ax.set_ylabel('Packet Size (Length)')
ax.set_title('Real-Time Packet Size Visualization')

def init():
    line.set_data([], [])
    return line,

def update(frame):
    current_time = df['Timestamp'].iloc[frame]
    current_data = df[df['Timestamp'] <= current_time]
    line.set_data(current_data['Timestamp'], current_data['Length'])
    ax.set_xlim(current_data['Timestamp'].min(), current_time)
    return line,

ani = FuncAnimation(fig, update, frames=len(df), init_func=init, blit=True, repeat=False)
plt.show()
