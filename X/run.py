import pyshark

def capture_and_analyze(interface):
    capture = pyshark.LiveCapture(interface=interface)
    # Start capturing packets
    print(f"Capturing packets on interface {interface}. Press Ctrl+C to stop...")
    try:
        for packet in capture.sniff_continuously():
            # Process the packet here
            print(packet)  # Print packet details (customize this based on your analysis needs)
    except KeyboardInterrupt:
        print("Stopping capture...")

if __name__ == "__main__":
    # Specify the network interface to capture packets (use 'Wi-Fi' or 'Ethernet' interface)
    interface = "Wi-Fi"  # Update this with your actual interface name

    capture_and_analyze(interface)
