import matplotlib.pyplot as plt
import pandas as pd


def plot_packet_data(df):
    # Step 2: Set the 'Timestamp' column as the index
    df.set_index('Timestamp', inplace=True)

    # Step 3: Resample the data to 1-second intervals and sum the packet sizes
    df_resampled = df.resample('10s').sum().fillna(0).reset_index()

    # Step 4: Plot the data as a line chart
    plt.figure(figsize=(12, 6))
    plt.plot(df_resampled['Timestamp'], df_resampled['Length'], label='Packet Size', color='blue', linestyle='-',
             marker='o')
    plt.xlabel('Timestamp')
    plt.ylabel('Packet Size (Summed per Second)')
    plt.title('Packet Size Over Time')
    plt.legend()
    plt.grid(True)
    plt.show()


if __name__ == '__main__':
    df = pd.read_csv('packet_data.csv', parse_dates=['Timestamp'])
    plot_packet_data(df)
