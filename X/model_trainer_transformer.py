import numpy as np
from keras import Input, Model
from keras.optimizers import Adam
from keras.saving.save import load_model
from sklearn.preprocessing import MinMaxScaler
from keras.models import Sequential, save_model
from keras.layers import MultiHeadAttention, LayerNormalization, Dropout, Dense, Flatten
from keras.callbacks import EarlyStopping, ReduceLROnPlateau
import joblib
import logging
from data_handler import read_data
import pandas as pd

# Configure logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s %(levelname)s %(message)s')

model_file = 'trained_model.h5'
scaler_file = 'scaler.gz'
sequence_length = 5

def create_sequences(data, seq_length):
    xs, ys = [], []
    for i in range(len(data) - seq_length):
        x = data['Length'].iloc[i:i + seq_length].astype(float)
        y = data['Length'].iloc[i + seq_length].astype(float)
        xs.append(x)
        ys.append(y)
    return np.array(xs), np.array(ys)

def create_transformer_encoder(input_shape, head_size, num_heads, ff_dim, dropout=0.1):
    inputs = Input(shape=input_shape)
    x = MultiHeadAttention(key_dim=head_size, num_heads=num_heads)(inputs, inputs)
    x = Dropout(dropout)(x)
    x = LayerNormalization(epsilon=1e-6)(x)
    res = x + inputs

    x = Dense(ff_dim, activation="relu")(res)
    x = Dropout(dropout)(x)
    x = Dense(input_shape[-1])(x)
    outputs = LayerNormalization(epsilon=1e-6)(x)
    return Model(inputs, outputs)

def create_model():
    input_shape = (sequence_length, 1)
    transformer_encoder = create_transformer_encoder(input_shape, head_size=256, num_heads=4, ff_dim=64, dropout=0.1)

    model = Sequential([
        transformer_encoder,
        Flatten(),
        Dense(64, activation="relu"),
        Dropout(0.1),
        Dense(1)
    ])
    model.compile(optimizer=Adam(learning_rate=0.001), loss='mse')

    return model

def train_and_save_model(file='TrainData.csv', epochs=100, validation_split=0.2):
    logging.info("Training model...")
    df = read_data(file)
    if df.empty:
        logging.error("No data available for training")
        return

    df_resampled = df.resample('10S', on='Timestamp').sum().fillna(0).reset_index()

    scaler = MinMaxScaler()
    df_resampled['Length'] = scaler.fit_transform(df_resampled[['Length']])
    X, y = create_sequences(df_resampled, sequence_length)
    X = X.reshape((X.shape[0], X.shape[1], 1))

    model = create_model()

    # Callbacks for early stopping and learning rate reduction
    early_stopping = EarlyStopping(monitor='val_loss', patience=10, restore_best_weights=True)
    reduce_lr = ReduceLROnPlateau(monitor='val_loss', factor=0.2, patience=5, min_lr=0.0001)

    history = model.fit(X, y, epochs=epochs, verbose=1, validation_split=validation_split,
                        callbacks=[early_stopping, reduce_lr])

    save_model(model, model_file)
    joblib.dump(scaler, scaler_file)
    logging.info("Model trained and saved.")

def fine_tune_model(file='TrainData.csv', epochs=50, validation_split=0.2):
    logging.info("Fine-tuning model...")
    df = read_data(file)
    if df.empty:
        logging.error("No data available for fine-tuning")
        return

    try:
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        df_resampled = df.resample('10S', on='Timestamp').sum().fillna(0).reset_index()

        scaler = joblib.load(scaler_file)
        df_resampled['Length'] = scaler.transform(df_resampled[['Length']])
        X, y = create_sequences(df_resampled, sequence_length)
        X = X.reshape((X.shape[0], X.shape[1], 1))

        model = load_model(model_file)
        original_loss = model.evaluate(X, y, verbose=0)
        logging.info(f"Original model loss: {original_loss}")

        # Callbacks for early stopping and learning rate reduction
        early_stopping = EarlyStopping(monitor='val_loss', patience=10, restore_best_weights=True)
        reduce_lr = ReduceLROnPlateau(monitor='val_loss', factor=0.2, patience=5, min_lr=0.0001)

        history = model.fit(X, y, epochs=epochs, verbose=1, validation_split=validation_split,
                            callbacks=[early_stopping, reduce_lr])

        fine_tuned_loss = model.evaluate(X, y, verbose=0)
        logging.info(f"Fine-tuned model loss: {fine_tuned_loss}")

        if fine_tuned_loss < original_loss:
            save_model(model, model_file)
            joblib.dump(scaler, scaler_file)
            logging.info("Fine-tuned model saved as it performs better.")
        else:
            logging.info("Fine-tuned model did not perform better, not saved.")
    except Exception as e:
        logging.error(f"Error in fine-tuning model: {e}")

def predict(file='TestData.csv'):
    logging.info("Testing model...")
    df = read_data(file)
    if df.empty:
        logging.error("No data available for testing")
        return

    df['Timestamp'] = pd.to_datetime(df['Timestamp'])
    df_resampled = df.resample('10S', on='Timestamp').sum().fillna(0).reset_index()

    scaler = joblib.load(scaler_file)
    df_resampled['Length'] = scaler.transform(df_resampled[['Length']])
    X, y = create_sequences(df_resampled, sequence_length)
    X = X.reshape((X.shape[0], X.shape[1], 1))

    model = load_model(model_file)
    predictions = model.predict(X)

    # Inverse transform predictions
    predictions = scaler.inverse_transform(predictions)
    y_true = scaler.inverse_transform(y.reshape(-1, 1))

    mse = np.mean((predictions - y_true)**2)
    mae = np.mean(np.abs(predictions - y_true))

    logging.info(f"Model testing completed. MSE: {mse}, MAE: {mae}")
    print(f"MSE: {mse}")
    print(f"MAE: {mae}")

    return predictions, y_true

if __name__ == '__main__':
    file = 'TrainData.csv'
    train_and_save_model()
    fine_tune_model()
    # predict(file='TestData.csv')
