import matplotlib.pyplot as plt
import logging
from data_handler import read_data
import joblib
from keras.models import load_model
from sklearn.metrics import mean_squared_error, mean_absolute_error
import numpy as np
import pandas as pd

logging.basicConfig(level=logging.INFO, format='%(asctime)s %(message)s')
model_file = 'trained_model.h5'
scaler_file = 'scaler.gz'
sequence_length = 5
prediction_steps = 30  # Predicting for next 5 minutes (5 * 60 seconds / 10 seconds per step)

def create_sequences(data, seq_length):
    xs, ys = [], []
    for i in range(len(data) - seq_length):
        x = data['Length'].iloc[i:i + seq_length].astype(float)
        y = data['Length'].iloc[i + seq_length].astype(float)
        xs.append(x)
        ys.append(y)
    return np.array(xs), np.array(ys)

def plot_predictions(df, y_true, y_pred, extended_predictions, extended_timestamps):
    plt.figure(figsize=(10, 6))
    plt.plot(df['Timestamp'][:len(y_true)], y_true, label='Actual')
    plt.plot(df['Timestamp'][:len(y_pred)], y_pred, label='Predicted')
    if extended_predictions is not None:
        plt.plot(extended_timestamps, extended_predictions, label='Extended Predictions', linestyle='--')
    plt.xlabel('Time')
    plt.ylabel('Length')
    plt.title('Actual vs Predicted Values')
    plt.legend()
    plt.show()

def extend_predictions(model, last_sequence, steps, last_timestamp, interval):
    extended_predictions = []
    extended_timestamps = []
    current_sequence = last_sequence
    current_timestamp = last_timestamp
    for _ in range(steps):
        prediction = model.predict(current_sequence.reshape(1, sequence_length, 1))
        extended_predictions.append(prediction[0, 0])
        current_sequence = np.append(current_sequence[1:], prediction)
        current_timestamp += pd.Timedelta(seconds=interval)
        extended_timestamps.append(current_timestamp)
    return extended_predictions, extended_timestamps

def test_model():
    logging.info("Testing model...")
    df = read_data()
    if df.empty:
        logging.error("No data available for testing")
        return

    df_resampled = df.resample('10s', on='Timestamp').sum().fillna(0).reset_index()

    # Load scaler and transform data
    scaler = joblib.load(scaler_file)
    df_resampled['Length'] = scaler.transform(df_resampled[['Length']])
    X, y = create_sequences(df_resampled, sequence_length)
    X = X.reshape((X.shape[0], X.shape[1], 1))

    # Load model
    model = load_model(model_file)

    # Make predictions
    predictions = model.predict(X)

    # Calculate MSE and MAE
    mse = mean_squared_error(y, predictions)
    mae = mean_absolute_error(y, predictions)

    logging.info(f"Model testing completed. MSE: {mse}, MAE: {mae}")
    print(f"MSE: {mse}")
    print(f"MAE: {mae}")

    # Extend predictions for the next 5 minutes
    last_sequence = X[-1].reshape(sequence_length)
    last_timestamp = df_resampled['Timestamp'].iloc[-1]
    extended_predictions, extended_timestamps = extend_predictions(model, last_sequence, prediction_steps, last_timestamp, 10)

    # Plot predictions
    plot_predictions(df_resampled, y, predictions, extended_predictions, extended_timestamps)

if __name__ == '__main__':
    print("Testing model...")
    test_model()
