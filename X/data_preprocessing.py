import pandas as pd
from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error
import datetime
import matplotlib.pyplot as plt

# Sample data
data = {
    "Timestamp": ["2024-05-22 15:21:21.282858", "2024-05-22 15:21:29.259571", "2024-05-22 15:21:29.259674",
                  "2024-05-22 15:21:29.259731", "2024-05-22 15:21:29.285160", "2024-05-22 15:21:29.516519",
                  "2024-05-22 15:21:29.516563", "2024-05-22 15:21:29.548908", "2024-05-22 15:22:15.012886",
                  "2024-05-22 15:22:15.012989", "2024-05-22 15:22:15.013037", "2024-05-22 15:22:15.037984",
                  "2024-05-22 15:23:36.307930", "2024-05-22 15:23:36.308068", "2024-05-22 15:23:36.308131",
                  "2024-05-22 15:23:36.372329"],
    "Length": [55, 1292, 1292, 1292, 1292, 1292, 1292, 1292, 1292, 1292, 1292, 1292, 1292, 1292, 1292, 1292]
}

df = pd.DataFrame(data)
df['Timestamp'] = pd.to_datetime(df['Timestamp'])

# Resample by second and sum packet lengths
df.set_index('Timestamp', inplace=True)
df_resampled = df.resample('S').sum().fillna(0).reset_index()

# Normalize the data
scaler = MinMaxScaler()
df_resampled['Length'] = scaler.fit_transform(df_resampled[['Length']])

# Split the data
X = df_resampled['Timestamp'].apply(lambda x: x.toordinal()).values.reshape(-1, 1)
y = df_resampled['Length'].values
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, shuffle=False)

# Train a model
model = LinearRegression()
model.fit(X_train, y_train)

# Evaluate the model
y_pred = model.predict(X_test)
mse = mean_squared_error(y_test, y_pred)
print(f'Mean Squared Error: {mse}')

# Predict the next network packet size
last_timestamp = df_resampled['Timestamp'].iloc[-1]
next_timestamp = last_timestamp + datetime.timedelta(seconds=1)
next_timestamp_ordinal = next_timestamp.toordinal()
next_packet_size = model.predict([[next_timestamp_ordinal]])
next_packet_size = scaler.inverse_transform(next_packet_size.reshape(-1, 1))

print(f'Predicted packet size for the next second: {next_packet_size[0][0]}')

# Plotting the actual and predicted data
plt.figure(figsize=(12, 6))
plt.plot(df_resampled['Timestamp'], scaler.inverse_transform(df_resampled['Length'].values.reshape(-1, 1)), label='Actual Data', color='blue')
plt.plot(df_resampled['Timestamp'][len(X_train):], scaler.inverse_transform(y_pred.reshape(-1, 1)), label='Predicted Data', color='red')
plt.xlabel('Timestamp')
plt.ylabel('Packet Length')
plt.title('Actual vs Predicted Network Packet Lengths')
plt.legend()
plt.grid(True)
plt.show()
