import pyshark
import threading
import csv

stop_sniffing = threading.Event()


def capture_packets(interface, capture_duration=10):
    capture = pyshark.LiveCapture(interface=interface)
    print(f"Capturing packets on interface {interface} for {capture_duration} seconds...")

    packet_data = []

    def sniff_packets():
        try:
            for packet in capture.sniff_continuously():
                if stop_sniffing.is_set():
                    break
                try:
                    src_ip = packet.ip.src
                    dst_ip = packet.ip.dst
                    protocol = packet.highest_layer
                    length = packet.length

                    packet_info = (src_ip, dst_ip, protocol, length)
                    print(f"Packet: {src_ip} -> {dst_ip}, Protocol: {protocol}, Length: {length}")

                    packet_data.append(packet_info)
                except AttributeError:
                    # Skip packets that don't have IP layer
                    continue
                except Exception as e:
                    print(f"An error occurred while processing packet: {e}")
        except Exception as e:
            print(f"An error occurred during capture: {e}")

    # Start the sniffing in a separate thread
    sniff_thread = threading.Thread(target=sniff_packets)
    sniff_thread.start()

    # Stop sniffing after the specified duration
    threading.Timer(capture_duration, stop_sniffing.set).start()
    sniff_thread.join()
    print("Stopped packet capture.")

    return packet_data


def save_packets_to_csv(packet_data, output_file):
    print("Time to save the packets")

    if packet_data:
        # Debugging output for packet data
        print(f"Captured {len(packet_data)} packets.")
        print(f"Saving packet data to {output_file}")
        try:
            with open(output_file, mode='w', newline='') as file:
                writer = csv.writer(file)
                writer.writerow(['Source IP', 'Destination IP', 'Protocol', 'Length'])  # CSV header
                writer.writerows(packet_data)
            print(f"File saved to {output_file}")
        except Exception as e:
            print(f"An error occurred while saving the file: {e}")
    else:
        print("No packet data captured.")


if __name__ == "__main__":
    # Specify the network interface to capture packets (use 'Wi-Fi' or 'Ethernet' interface)
    interface = "Wi-Fi"  # Update this with your actual interface name
    output_file = 'packet_data.csv'

    print(f"Output file path: {output_file}")
    packet_data = capture_packets(interface, capture_duration=10)
    save_packets_to_csv(packet_data, output_file)
    print(packet_data)
