import requests

data = {
    'field1': 'value1',
    'field2': 'value2',
    'field3': 'value3'
}

response = requests.post('http://localhost:5000/send_data', json=data)
print(response.text)
