import numpy as np
from keras.optimizers import Adam
from keras.saving.save import load_model
from sklearn.preprocessing import MinMaxScaler
from keras.models import Sequential, save_model
from keras.layers import Conv1D, MaxPooling1D, Flatten, RepeatVector, LSTM, Dense
import joblib
import logging
from data_handler import read_data
import pandas as pd
import matplotlib.pyplot as plt  # Import matplotlib for plotting

# Configure logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s %(levelname)s %(message)s')

model_file = 'trained_model.h5'
scaler_file = 'scaler.gz'
sequence_length = 5


def create_sequences(data, seq_length):
    xs, ys = [], []
    for i in range(len(data) - seq_length):
        x = data['Length'].iloc[i:i + seq_length].astype(float)
        print(f"Index {i}: x.shape = {x.shape}")  # Print the shape of x
        y = data['Length'].iloc[i + seq_length].astype(float)
        xs.append(x)
        ys.append(y)
    return np.array(xs), np.array(ys)


def create_model():
    model = Sequential([
        Conv1D(filters=64, kernel_size=2, activation='relu', input_shape=(sequence_length, 1)),
        MaxPooling1D(pool_size=2),
        Flatten(),
        RepeatVector(1),
        LSTM(25, activation='relu', return_sequences=True),
        LSTM(25, activation='relu'),
        Dense(1)
    ])
    model.compile(optimizer=Adam(learning_rate=0.01), loss='mse')  # Experiment with different learning rates

    return model


def train_and_save_model(file='packet_data.csv', epochs=100, validation_split=0.2):
    """Trains and saves the model with hyperparameter options.

    Args:
        epochs (int, optional): Number of epochs to train. Defaults to 10.
        validation_split (float, optional): Fraction of data for validation. Defaults to 0.2.
        :param file:
    """

    logging.info("Training model...")
    df = read_data(file)
    if df.empty:
        logging.error("No data available for training")
        return

    df_resampled = df.resample('10s', on='Timestamp').sum().fillna(0).reset_index()

    scaler = MinMaxScaler()
    df_resampled['Length'] = scaler.fit_transform(df_resampled[['Length']])
    X, y = create_sequences(df_resampled, sequence_length)
    print(X.shape)
    X = X.reshape((X.shape[0], X.shape[1], 1))

    model = create_model()
    # Early stopping can be implemented here (see comments below)

    history = model.fit(X, y, epochs=epochs, verbose=0, validation_split=validation_split)

    # Plot training and validation loss curves
    plt.figure(figsize=(12, 6))
    plt.plot(history.history['loss'], label='Training Loss')
    plt.plot(history.history['val_loss'], label='Validation Loss')
    plt.title('Training and Validation Loss')
    plt.xlabel('Epochs')
    plt.ylabel('Loss')
    plt.legend()
    plt.grid(True)
    plt.show()

    save_model(model, model_file)
    joblib.dump(scaler, scaler_file)
    logging.info("Model trained and saved.")


def fine_tune_model(epochs=50, validation_split=0.2):
    logging.info("Fine-tuning model...")
    df = read_data()
    if df.empty:
        logging.error("No data available for fine-tuning")
        return

    try:
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        df_resampled = df.resample('10S', on='Timestamp').sum().fillna(0).reset_index()

        scaler = joblib.load(scaler_file)
        df_resampled['Length'] = scaler.transform(df_resampled[['Length']])
        X, y = create_sequences(df_resampled, sequence_length)
        X = X.reshape((X.shape[0], X.shape[1], 1))

        model = load_model(model_file)
        original_loss = model.evaluate(X, y, verbose=0)
        logging.info(f"Original model loss: {original_loss}")

        history = model.fit(X, y, epochs=epochs, verbose=0, validation_split=validation_split)

        # Plot fine-tuning training and validation loss curves
        plt.figure(figsize=(12, 6))
        plt.plot(history.history['loss'], label='Fine-tuning Training Loss')
        plt.plot(history.history['val_loss'], label='Fine-tuning Validation Loss')
        plt.title('Fine-tuning Training and Validation Loss')
        plt.xlabel('Epochs')
        plt.ylabel('Loss')
        plt.legend()
        plt.grid(True)
        plt.show()

        fine_tuned_loss = model.evaluate(X, y, verbose=0)
        logging.info(f"Fine-tuned model loss: {fine_tuned_loss}")

        if fine_tuned_loss < original_loss:
            save_model(model, model_file)
            joblib.dump(scaler, scaler_file)
            logging.info("Fine-tuned model saved as it performs better.")
        else:
            logging.info("Fine-tuned model did not perform better, not saved.")
    except Exception as e:
        logging.error(f"Error in fine-tuning model: {e}")


if __name__ == '__main__':
    print("Training model...")
    file = 'TrainData.csv'
    # train_and_save_model(file)
    train_and_save_model()
    # fine_tune_model()
