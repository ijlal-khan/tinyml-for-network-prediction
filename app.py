from flask import Flask, render_template, send_file, jsonify
import io
import logging
import subprocess
from live_monitoring import build_graph, last_graph
from model_trainer_CNN_LSTM import fine_tune_model

app = Flask(__name__)


@app.route('/plot.png')
def plot_png():
    img, new_data = build_graph()
    if img:
        logging.info("New graph generated.")
        return send_file(io.BytesIO(img.getvalue()), mimetype='image/png')
    elif last_graph:
        logging.info("Using last graph.")
        return send_file(io.BytesIO(last_graph.getvalue()), mimetype='image/png')
    else:
        logging.error("No graph available.")
        return "No graph available", 204


@app.route('/status')
def status():
    _, new_data = build_graph()
    return jsonify({"new_data": new_data})


@app.route('/fine_tune', methods=['POST'])
def fine_tune():
    try:
        fine_tune_model()
        return jsonify({"status": "Model fine-tuned successfully"})
    except Exception as e:
        logging.error(f"Error during fine-tuning: {e}")
        return jsonify({"status": "Error during fine-tuning", "error": str(e)}), 500


@app.route('/start_capture', methods=['POST'])
def start_capture():
    try:
        # Start capture.py in a new process
        subprocess.Popen(['python', 'capture.py'])
        return jsonify({"status": "Capture started successfully"})
    except Exception as e:
        logging.error(f"Error starting capture: {e}")
        return jsonify({"status": "Error starting capture", "error": str(e)}), 500

@app.route('/')
def index():
    return render_template('index.html')


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)  # Set to DEBUG to see more detailed logs
    app.run(debug=True)
