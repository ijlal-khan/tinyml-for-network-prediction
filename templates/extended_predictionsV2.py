import os
import matplotlib.pyplot as plt
import logging
from data_handler import read_data
import joblib
from keras.models import load_model
from sklearn.metrics import mean_squared_error, mean_absolute_error
import numpy as np
import pandas as pd
import tensorflow as tf
import time

# Suppress TensorFlow and Keras messages
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
tf.get_logger().setLevel('ERROR')

logging.basicConfig(level=logging.INFO, format='%(asctime)s %(message)s')

# Define constants
model_file = 'trained_model.h5'
scaler_file = 'scaler.gz'
sequence_length = 5
prediction_steps = 15  # 5 minutes (15 steps of 10 seconds each)

# Load model and scaler once
model = load_model(model_file)
scaler = joblib.load(scaler_file)

# Initialize the last known state
last_sequence = None
last_timestamp = None
targets = None  # Initialize targets as a global variable


def create_sequences(data):
    sequences, targets = [], []
    for i in range(len(data) - sequence_length):
        sequence = data['Length'].iloc[i:i + sequence_length]
        target = data['Length'].iloc[i + sequence_length]
        sequences.append(sequence.astype(float))
        targets.append(target.astype(float))
    return np.array(sequences), np.array(targets)


def plot_predictions(df, true_values, predicted, extended_values=None, extended_timestamps=None):
    plt.figure(figsize=(10, 6))
    plt.plot(df['Timestamp'][:len(true_values)], true_values, label='Actual')
    plt.plot(df['Timestamp'][:len(predicted)], predicted, label='Predicted')
    if extended_values is not None:
        plt.plot(extended_timestamps, extended_values, label='Extended Predictions', linestyle='--')
    plt.xlabel('Time')
    plt.ylabel('Length')
    plt.title('Actual vs Predicted Values')
    plt.legend()
    plt.show()


def extend_predictions(model, last_sequence, steps, last_timestamp, interval):
    predictions, timestamps = [], []
    for _ in range(steps):
        prediction = model.predict(last_sequence.reshape(1, sequence_length, 1))
        predictions.append(prediction[0, 0])
        last_sequence = np.append(last_sequence[1:], prediction)
        last_timestamp += pd.Timedelta(seconds=interval)
        timestamps.append(last_timestamp)
    return predictions, timestamps


def update_graph():
    global last_sequence, last_timestamp, targets
    logging.info("Updating model...")

    df = read_data()
    if df.empty:
        logging.error("No data available for updating")
        return

    df_resampled = df.resample('10s', on='Timestamp').sum().fillna(0).reset_index()

    # Check if we need to initialize the last sequence and timestamp
    if last_sequence is None or last_timestamp is None:
        # Load scaler and transform data
        df_resampled['Length'] = scaler.transform(df_resampled[['Length']])
        sequences, targets = create_sequences(df_resampled)
        sequences = sequences.reshape((sequences.shape[0], sequences.shape[1], 1))
        predictions = model.predict(sequences)

        # Initialize the last known state
        last_sequence = sequences[-1].reshape(sequence_length)
        last_timestamp = df_resampled['Timestamp'].iloc[-sequence_length]
    else:
        # Process only new data
        new_data = df_resampled[df_resampled['Timestamp'] > last_timestamp]
        if not new_data.empty:
            new_data['Length'] = scaler.transform(new_data[['Length']])
            new_sequence = last_sequence.tolist() + new_data['Length'].tolist()
            last_sequence = np.array(new_sequence[-sequence_length:])
            predictions = model.predict(last_sequence.reshape(1, sequence_length, 1))

            # Update last timestamp
            last_timestamp = new_data['Timestamp'].iloc[-1]

    # Extend predictions
    extended_predictions, extended_timestamps = extend_predictions(model, last_sequence, prediction_steps, last_timestamp, 10)

    # Plot predictions
    plot_predictions(df_resampled, targets, predictions, extended_predictions, extended_timestamps)


if __name__ == '__main__':
    while True:
        update_graph()
        time.sleep(5)  # Wait for 5 seconds before next update
