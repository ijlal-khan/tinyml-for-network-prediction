import pyshark
import socket
import threading
import csv
import time
from datetime import datetime

# Global variable for the update interval
UPDATE_INTERVAL = 5

# Specify the path to TShark executable
pyshark.tshark.tshark.TSHARK_PATH = '/usr/bin/tshark'

stop_sniffing = threading.Event()
packet_data = []
output_file = 'packet_data.csv'
youtube_cidr = '74.125.205.0/24'

def get_local_ip():
    """Get the local IP address of the machine."""
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(('8.8.8.8', 80))
        local_ip = s.getsockname()[0]
        s.close()
        return local_ip
    except Exception as e:
        print(f"Error getting local IP address: {e}")
        return None

def capture_packets(network_interface, local_ip_addr):
    """Capture packets and save them to a CSV file."""
    global packet_data
    capture_filter = f'(http or ssl)'
    capture = pyshark.LiveCapture(interface=network_interface, display_filter=capture_filter)
    print(f"Capturing packets on interface {network_interface}. Press Enter to stop...")

    def sniff_packets():
        """Sniff packets and process them."""
        try:
            for packet in capture.sniff_continuously():
                if stop_sniffing.is_set():
                    break
                try:
                    timestamp = packet.sniff_time
                    port = packet.transport_layer
                    src_ip = packet.ip.src
                    dst_ip = packet.ip.dst
                    protocol = packet.highest_layer
                    length = packet.length

                    packet_info = (timestamp, src_ip, dst_ip, protocol, length)
                    print(f"Packet: {timestamp}, {src_ip} -> {dst_ip}, Protocol: {protocol}, Length: {length}, Port: {port}")

                    packet_data.append(packet_info)
                except AttributeError:
                    continue
                except Exception as e:
                    print(f"Error processing packet: {e}")
        except Exception as e:
            print(f"Error during capture: {e}")

    sniff_thread = threading.Thread(target=sniff_packets)
    sniff_thread.start()

    save_thread = threading.Thread(target=periodic_save)
    save_thread.start()

    input()
    stop_sniffing.set()
    sniff_thread.join()
    save_thread.join()
    print("Stopped packet capture.")

def save_packets_to_csv(packet_data, output_file):
    """Save packet data to a CSV file."""
    if packet_data:
        print(f"Captured {len(packet_data)} packets.")
        print(f"Saving packet data to {output_file}")
        try:
            with open(output_file, mode='w', newline='') as file:
                writer = csv.writer(file)
                writer.writerow(['Timestamp', 'SourceIP', 'DestinationIP', 'Protocol', 'Length'])
                writer.writerows(packet_data)
            print(f"File saved to {output_file}")
        except Exception as e:
            print(f"Error saving file: {e}")
    else:
        print("No packet data captured.")

def periodic_save():
    """Periodically save packet data to CSV."""
    while not stop_sniffing.is_set():
        time.sleep(UPDATE_INTERVAL)
        save_packets_to_csv(packet_data, output_file)

if __name__ == "__main__":
    interface = "Wi-Fi"  # Update this with your actual interface name
    local_ip = get_local_ip()
    if local_ip:
        print(f"Local IP address: {local_ip}")
        print(f"Output file path: {output_file}")
        capture_packets(interface, local_ip)
    else:
        print("Could not determine local IP address.")
